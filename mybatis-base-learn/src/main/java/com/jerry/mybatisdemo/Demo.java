package com.jerry.mybatisdemo;

import com.jerry.mybatisdemo.entity.Poem;
import com.jerry.mybatisdemo.mapper.PoemMapper;
import com.jerry.mybatisdemo.utils.SqlSessionFactoryUtil;
import org.apache.ibatis.session.SqlSession;

import java.util.List;

/**
 * @author jerryjin
 * @Classname Main
 * @Description TODO
 * @Date 2021-04-23 9:12
 */
public class Demo {

    /**
     * 取出 mapper 进行查询
     * @param id
     * @return
     */
    private static Poem queryById(Long id){
        try (SqlSession session = SqlSessionFactoryUtil.getFactory().openSession()) {
            PoemMapper mapper = session.getMapper(PoemMapper.class);
            return mapper.queryById(id);
        }
    }
    /**
     * 直接使用 selectList 方法配合 com.jerry.mybatisdemo.mapper.PoemMapper.queryAll 进行查询
     * @return
     */
    private static List<Poem> queryAll(){
        try (SqlSession session = SqlSessionFactoryUtil.getFactory().openSession()) {
            List<Poem> list = session.selectList("com.jerry.mybatisdemo.mapper.PoemMapper.queryAll");
            return list;
        }
    }

    public static void main(String[] args) {
        Poem poem = queryById(1L);
        System.out.println(poem);

        List<Poem> poems = queryAll();
        System.out.println(poems);
    }
    
}
