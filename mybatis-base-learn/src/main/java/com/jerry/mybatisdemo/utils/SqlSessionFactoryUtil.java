package com.jerry.mybatisdemo.utils;

import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

import java.io.IOException;
import java.io.InputStream;

/**
 * @author jerryjin
 * @Classname SqlSessionFactory
 * @Description SqlSessionFactory 单例
 * @Date 2021-04-23 16:22
 */
public class SqlSessionFactoryUtil {

    private volatile static SqlSessionFactory singleton;
    private SqlSessionFactoryUtil(){}

    public static SqlSessionFactory getFactory() {
        if (singleton == null) {
            synchronized (SqlSessionFactoryUtil.class) {
                if (singleton == null) {
                    try {
                        InputStream inputStream = Resources.getResourceAsStream("mybatis/mybatis-config.xml");
                        singleton = new SqlSessionFactoryBuilder().build(inputStream);

                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
        return singleton;
    }

}
