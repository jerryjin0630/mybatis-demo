package com.jerry.mybatisdemo.typehandler;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONException;
import org.apache.ibatis.type.BaseTypeHandler;
import org.apache.ibatis.type.JdbcType;
import org.apache.ibatis.type.MappedJdbcTypes;
import org.apache.ibatis.type.MappedTypes;

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * @author jerryjin
 * @Classname JSONArrayHandler
 * @Description JSONArray解析
 * @Date 2021-04-23 17:42
 */
@MappedTypes(JSONArray.class)
@MappedJdbcTypes(JdbcType.VARCHAR)
public class JSONArrayHandler extends BaseTypeHandler<JSONArray> {

    public JSONArray getResult(String jsonString) throws SQLException {
        if(jsonString != null){
            JSONArray jsonArray;
            try{
                jsonArray = JSONArray.parseArray(jsonString);
            }catch (JSONException ex){
                throw new SQLException("jsonString >> JSONArray 失败，jsonString:" + jsonString);
            }
            return jsonArray;
        }
        return null;
    }

    @Override
    public void setNonNullParameter(PreparedStatement preparedStatement, int parameterIndex, JSONArray jsonArray, JdbcType jdbcType) throws SQLException {
        preparedStatement.setString(parameterIndex, jsonArray.toJSONString());
    }

    @Override
    public JSONArray getNullableResult(ResultSet resultSet, String columnLabel) throws SQLException {
        return getResult(resultSet.getString(columnLabel));
    }

    @Override
    public JSONArray getNullableResult(ResultSet resultSet, int columnIndex) throws SQLException {
        return getResult(resultSet.getString(columnIndex));
    }

    @Override
    public JSONArray getNullableResult(CallableStatement callableStatement, int parameterIndex) throws SQLException {
        return getResult(callableStatement.getString(parameterIndex));
    }

}
