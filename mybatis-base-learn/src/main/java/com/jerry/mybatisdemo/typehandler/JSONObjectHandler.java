package com.jerry.mybatisdemo.typehandler;

import com.alibaba.fastjson.JSONException;
import com.alibaba.fastjson.JSONObject;
import org.apache.ibatis.type.*;

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * 传入参数时 java类型 >> JDBC类型，获取结果时 java类型 << JDBC类型
 * @author jerryjin
 * @Classname JsonTypeHandler
 * @Description JSON解析
 * @Date 2021-04-23 16:02
 */
@MappedTypes(JSONObject.class)
@MappedJdbcTypes(JdbcType.VARCHAR)
public class JSONObjectHandler implements TypeHandler<JSONObject> {

    private JSONObject getResult(String jsonString) throws SQLException {
        if(jsonString != null){
            JSONObject jsonObject;
            try{
                jsonObject = JSONObject.parseObject(jsonString);
            }catch (JSONException ex){
                throw new SQLException("jsonString >> JSON 失败，jsonString:" + jsonString);
            }
            return jsonObject;
        }
        return null;
    }

    /**
     *  设置参数时 java类型 转 JDBC类型
     */
    @Override
    public void setParameter(PreparedStatement preparedStatement, int parameterIndex, JSONObject jsonObject, JdbcType jdbcType) throws SQLException {
        preparedStatement.setString(parameterIndex, jsonObject.toJSONString());
    }
    /**
     *  从结果集按列名取对象时 JdbcType.VARCHAR >> Java类型
     */
    @Override
    public JSONObject getResult(ResultSet resultSet, String columnLabel) throws SQLException {
        return getResult(resultSet.getString(columnLabel));
    }
    /**
     *  从结果集按索引取对象时 JdbcType.VARCHAR >> Java类型
     */
    @Override
    public JSONObject getResult(ResultSet resultSet, int columnIndex) throws SQLException {
        return getResult(resultSet.getString(columnIndex));
    }

    /**
     * 针对存储过程转换结果
     */
    @Override
    public JSONObject getResult(CallableStatement callableStatement, int parameterIndex) throws SQLException {
        return getResult(callableStatement.getString(parameterIndex));
    }

}