package com.jerry.mybatisdemo.mapper;

import com.jerry.mybatisdemo.entity.Poem;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

/**
 * 笑虾原创诗词表(Poem)表数据库访问层
 *
 * @author jerryjin
 * @since 2021-04-23 16:40:52
 */
@Mapper
public interface PoemMapper {

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    Poem queryById(Object id);

    /**
     * 查询指定行数据
     *
     * @param offset 查询起始位置
     * @param limit  查询条数
     * @return 对象列表
     */
    List<Poem> queryAllByLimit(@Param("offset") int offset, @Param("limit") int limit);


    /**
     * 通过实体作为筛选条件查询
     *
     * @param poem 实例对象
     * @return 对象列表
     */
    List<Poem> queryAll(Poem poem);

    /**
     * 按条件【统计】列表，支持：datetiemBegin、datetiemEnd
     *
     * @param paramMap 参数 map
     * @return 对象列表
     */
    Integer countListByParam(Map<String, Object> paramMap);

    /**
     * 按条件【查询】列表，支持：datetiemBegin、datetiemEnd
     *
     * @param paramMap 参数 map
     * @return 对象列表
     */
    List<Poem> queryListByParam(Map<String, Object> paramMap);

    /**
     * 新增数据
     *
     * @param poem 实例对象
     * @return 影响行数
     */
    int insert(Poem poem);

    /**
     * 修改数据
     *
     * @param poem 实例对象
     * @return 影响行数
     */
    int update(Poem poem);

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 影响行数
     */
    int deleteById(Object id);

}

