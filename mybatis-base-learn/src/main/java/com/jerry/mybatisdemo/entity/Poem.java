package com.jerry.mybatisdemo.entity;

import com.alibaba.fastjson.JSONObject;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 笑虾原创诗词表(Poem)实体类
 *
 * @author jerryjin
 * @since 2021-04-23 16:40:51
 */
@Data
public class Poem implements Serializable {
    private static final long serialVersionUID = -88710667606745594L;
    /**
     * 主键
     */
    private Object id;
    /**
     * 诗词标题
     */
    private String title;
    /**
     * 诗词内容
     */
    private String content;
    /**
     * 作者
     */
    private String author;
    /**
     * 发表时间
     */
    private Date publishedTime;
    /**
     * 更新时间
     */
    private Date updateTime;
    /**
     * 创建时间
     */
    private Date createTime;

    private JSONObject jsonData;

//    @Override
//    public String toString() {
//        return title  + " ——" + author + '\n' +  content + '\n';
//    }

}
