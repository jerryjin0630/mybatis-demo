package com.mybatis.demo.controller;

import com.mybatis.demo.DemoApplication;
import com.mybatis.demo.entity.Poem;
import com.mybatis.demo.mapper.PoemMapper;
import com.mybatis.demo.service.PoemService;
import org.junit.jupiter.api.Test;
import org.mockito.BDDMockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

@SpringBootTest(classes = DemoApplication.class)
@AutoConfigureMockMvc
//@RunWith(SpringRunner.class)
//@WebMvcTest(PoemController.class)
class PoemControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private PoemService poemService;

    @Test
    public void selectOne() throws Exception {

        Poem poem = new Poem();
        poem.setId(1L);
        poem.setTitle("痴情癫");
        poem.setAuthor("笑虾");
        poem.setContent("<p>多情黯叹痴情癫，痴情苦笑多情难。</p><p>相思自古无良药，从来独步赴黄泉。</p>");

        BDDMockito.given(poemService.queryById(1L)).willReturn(poem);

        MockHttpServletRequestBuilder request = MockMvcRequestBuilders.get("/poem/get/1")
                .accept("application/json;charset=UTF-8");
        MvcResult mvcResult = mockMvc.perform(request)
                .andExpect(MockMvcResultMatchers.status().isOk()) // 是否 200
                .andDo(MockMvcResultHandlers.print()) // 打印结果
                .andReturn(); // 返回结果对象

//        mvcResult.getResponse().setCharacterEncoding("UTF-8"); // 也可以在拿到结果后再设置编码
        System.out.println("结果：" + mvcResult.getResponse().getContentAsString());

    }
}

//    @Test
//    void selectListByParam() {
//    }
//
//    @Test
//    void insert() {
//    }
//
//    @Test
//    void update() {
//    }
//
//    @Test
//    void deleteById() {
//    }
//
//    @org.junit.Test
//    public void testSelectOne() {
//    }
//
//    @org.junit.Test
//    public void testSelectListByParam() {
//    }
//
//    @org.junit.Test
//    public void testInsert() {
//    }
//
//    @org.junit.Test
//    public void testUpdate() {
//    }
//
//    @org.junit.Test
//    public void testDeleteById() {
//    }
