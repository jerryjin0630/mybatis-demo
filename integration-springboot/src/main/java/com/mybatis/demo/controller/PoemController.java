package com.mybatis.demo.controller;

import com.mybatis.demo.entity.Poem;
import com.mybatis.demo.service.PoemService;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

/**
 * (Poem)表控制层
 *
 * @author jerryjin
 * @since 2020-08-02 19:55:26
 */
@RestController
@RequestMapping("poem")
public class PoemController {
    /**
     * 服务对象
     */
    @Resource
    private PoemService poemService;

    /**
     * 通过主键查询单条数据
     *
     * @param id 主键
     * @return 单条数据
     */
    @GetMapping("get/{id}")
    public Poem selectOne(@PathVariable Long id) {
        return this.poemService.queryById(id);
    }

    /**
     * 按条件查询
     *
     * @param paramMap = Poem + datetiemBegin、datetiemEnd
     * @return 对象列表
     */
    @GetMapping("list")
    public List<Poem> selectListByParam(@RequestParam Map<String, Object> paramMap) {
        return this.poemService.queryListByParam(paramMap);
    }

    /**
     * 创建对象
     *
     * @param poem
     * @return 新对象主键
     */
    @PostMapping
    public Long insert(Poem poem) {
        Poem obj = this.poemService.insert(poem);
        return obj.getId();
    }

    /**
     * 更新对象
     *
     * @param poem
     * @return 更新后的对象
     */
    @PutMapping
    public Poem update(Poem poem) {
        return this.poemService.update(poem);
    }

    /**
     * 删除对象
     *
     * @param id 主键
     * @return 是否成功 bool
     */
    @DeleteMapping
    public boolean deleteById(Long id) {
        return this.poemService.deleteById(id);
    }


}
