package com.mybatis.demo.service.impl;

import com.mybatis.demo.entity.Poem;
import com.mybatis.demo.mapper.PoemMapper;
import com.mybatis.demo.service.PoemService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

/**
 * (Poem)表服务实现类
 *
 * @author jerryjin
 * @since 2020-08-02 19:46:37
 */
@Service("poemService")
public class PoemServiceImpl implements PoemService {
    @Resource
    private PoemMapper poemMapper;

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    @Override
    public Poem queryById(Long id) {
        return this.poemMapper.queryById(id);
    }

    /**
     * 查询多条数据
     *
     * @param offset 查询起始位置
     * @param limit  查询条数
     * @return 对象列表
     */
    @Override
    public List<Poem> queryAllByLimit(int offset, int limit) {
        return this.poemMapper.queryAllByLimit(offset, limit);
    }

    /**
     * 按条件查询列表，支持：datetiemBegin、datetiemEnd
     *
     * @param paramMap 参数 map
     * @return 对象列表
     */
    @Override
    public List<Poem> queryListByParam(Map<String, Object> paramMap) {
        return this.poemMapper.queryListByParam(paramMap);
    }

    /**
     * 新增数据
     *
     * @param poem 实例对象
     * @return 实例对象
     */
    @Override
    public Poem insert(Poem poem) {
        this.poemMapper.insert(poem);
        return poem;
    }

    /**
     * 修改数据
     *
     * @param poem 实例对象
     * @return 实例对象
     */
    @Override
    public Poem update(Poem poem) {
        this.poemMapper.update(poem);
        return this.queryById(poem.getId());
    }

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 是否成功
     */
    @Override
    public boolean deleteById(Long id) {
        return this.poemMapper.deleteById(id) > 0;
    }
}
