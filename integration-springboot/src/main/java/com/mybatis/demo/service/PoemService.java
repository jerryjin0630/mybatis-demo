package com.mybatis.demo.service;

import com.mybatis.demo.entity.Poem;

import java.util.List;
import java.util.Map;

/**
 * (Poem)表服务接口
 *
 * @author jerryjin
 * @since 2020-08-02 19:46:37
 */
public interface PoemService {

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    Poem queryById(Long id);

    /**
     * 查询多条数据
     *
     * @param offset 查询起始位置
     * @param limit  查询条数
     * @return 对象列表
     */
    List<Poem> queryAllByLimit(int offset, int limit);

    /**
     * 按条件查询列表，支持：datetiemBegin、datetiemEnd
     *
     * @param paramMap 参数 map
     * @return 对象列表
     */
    List<Poem> queryListByParam(Map<String, Object> paramMap);

    /**
     * 新增数据
     *
     * @param poem 实例对象
     * @return 实例对象
     */
    Poem insert(Poem poem);

    /**
     * 修改数据
     *
     * @param poem 实例对象
     * @return 实例对象
     */
    Poem update(Poem poem);

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 是否成功
     */
    boolean deleteById(Long id);

}
