package com.mybatis.demo.entity;

import java.io.Serializable;
import java.util.Date;

/**
 * (Poem)实体类
 *
 * @author jerryjin
 * @since 2020-08-02 19:46:37
 */
public class Poem implements Serializable {
    private static final long serialVersionUID = 526163010349043784L;
    /**
     * 主键
     */
    private Long id;
    /**
     * 诗词标题
     */
    private String title;
    /**
     * 诗词内容
     */
    private String content;
    /**
     * 作者
     */
    private String author;
    /**
     * 更新时间
     */
    private Date updateTime;
    /**
     * 创建时间
     */
    private Date createTime;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

}
